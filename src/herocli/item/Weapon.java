package herocli.item;

public class Weapon extends Item {

    //enum to contain all the different weapon types
    public enum WeaponType {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    //fields to contain damage, attack speed, and DPS
    private int damage;
    private double attackSpeed;
    private double weaponDPS = damage * attackSpeed;

    //initialize the weaponType enum
    public WeaponType weaponType;

    //getter for weapon DPS
    public double getWeaponDPS() {
        return weaponDPS;
    }

    //constructor using all class fields and superclass fields
    public Weapon(String name, int requiredLevel, Slot slot, WeaponType weaponType, double attackSpeed,
                  int damage) {
        super(name, requiredLevel, slot);
        this.weaponType = weaponType;
        this.attackSpeed = attackSpeed;
        this.damage = damage;
        this.weaponDPS = damage * attackSpeed;
    }

}

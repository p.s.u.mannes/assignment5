package herocli.item.lists;

import herocli.item.Armor;
import herocli.item.Item;

import java.util.HashMap;
import java.util.Map;

public class ArmorList {

    //Available types include:
    //• Cloth
    //• Leather
    //• Mail
    //• Plate

    //creating hashmap to store armor in and display to user
    public HashMap<Integer, Armor> armorList = new HashMap<>();

    public ArmorList() {
        //• Cloth
        //• Cloth Helmet
        Armor levelOneClothHelmet = new Armor("Wizard's Bandanna", 1,
                Item.Slot.HEAD, Armor.ArmorType.CLOTH,
                6,3,4,23);

        Armor levelTwoClothHelmet = new Armor("Dwarf Worker's Bobble", 2,
                Item.Slot.HEAD, Armor.ArmorType.CLOTH,
                34,4,9,53);

        Armor levelThreeClothHelmet = new Armor("Adept Lightning Hat", 3,
                Item.Slot.HEAD, Armor.ArmorType.CLOTH,
                55,9,9,78);

        //• Cloth Legs
        Armor levelOneClothLegs = new Armor("Wizard Breaches", 1,
                Item.Slot.LEGS, Armor.ArmorType.CLOTH,
                6,3,4,21);

        Armor levelTwoClothLegs = new Armor("Dwarf Party Pants", 2,
                Item.Slot.LEGS, Armor.ArmorType.CLOTH,
                34,4,9,34);

        Armor levelThreeClothLegs = new Armor("Adept Lightning Robes", 3,
                Item.Slot.LEGS, Armor.ArmorType.CLOTH,
                55,9,9,79);

        //• Cloth Body
        Armor levelOneClothBody = new Armor("Wizard Shirt", 1,
                Item.Slot.BODY, Armor.ArmorType.CLOTH,
                6,3,4,21);

        Armor levelTwoClothBody = new Armor("Rugged Acolyte Shirt", 2,
                Item.Slot.BODY, Armor.ArmorType.CLOTH,
                34,4,9,34);

        Armor levelThreeClothBody = new Armor("Adept Centrifugal Garb", 3,
                Item.Slot.BODY, Armor.ArmorType.CLOTH,
                55,9,9,79);

        //• Leather
        //• Leather Helmet
        Armor levelOneLeatherHelmet = new Armor("Pacifist's Blindfold", 1,
                Item.Slot.HEAD, Armor.ArmorType.LEATHER,
                6,3,14,4);

        Armor levelTwoLeatherHelmet = new Armor("Twisted Wood Runner's Hat", 2,
                Item.Slot.HEAD, Armor.ArmorType.LEATHER,
                34,4,35,5);

        Armor levelThreeLeatherHelmet = new Armor("Beast's Leathery Monocle", 3,
                Item.Slot.HEAD, Armor.ArmorType.LEATHER,
                55,9,195,6);

        //• Cloth Legs
        Armor levelOneLeatherLegs = new Armor("Rugged Leggings", 1,
                Item.Slot.LEGS, Armor.ArmorType.LEATHER,
                6,3,23,4);

        Armor levelTwoLeatherLegs = new Armor("Poorly Stitched Prince's Garbs", 2,
                Item.Slot.LEGS, Armor.ArmorType.LEATHER,
                34,4,43,6);

        Armor levelThreeLeatherLegs = new Armor("Elven Unicorn Leather Chaps", 3,
                Item.Slot.LEGS, Armor.ArmorType.LEATHER,
                55,9,123,7);

        //• Cloth Body
        Armor levelOneLeatherBody = new Armor("Pig-leather Butcher's Jacket", 1,
                Item.Slot.BODY, Armor.ArmorType.LEATHER,
                6,3,32,8);

        Armor levelTwoLeatherBody = new Armor("Shadow-touched Hunter's Jacket", 2,
                Item.Slot.BODY, Armor.ArmorType.LEATHER,
                34,4,44,9);

        Armor levelThreeLeatherBody = new Armor("Moonlit herocli.hero.characters.Ranger's Jacket", 3,
                Item.Slot.BODY, Armor.ArmorType.LEATHER,
                55,9,119,11);

        //• Mail
        //• Mail Helmet
        Armor levelOneMailHelmet = new Armor("Squire's Chain Helmet", 1,
                Item.Slot.HEAD, Armor.ArmorType.MAIL,
                6,35,11,4);

        Armor levelTwoMailHelmet = new Armor("Barbarian Helmet", 2,
                Item.Slot.HEAD, Armor.ArmorType.MAIL,
                34,41,21,5);

        Armor levelThreeMailHelmet = new Armor("Desert herocli.hero.characters.Warrior Helmet", 3,
                Item.Slot.HEAD, Armor.ArmorType.MAIL,
                55,91,42,6);

        //• Mail Legs
        Armor levelOneMailLegs = new Armor("Squire's Legs", 1,
                Item.Slot.LEGS, Armor.ArmorType.MAIL,
                6,31,23,4);

        Armor levelTwoMailLegs = new Armor("Barbarian Legs", 2,
                Item.Slot.LEGS, Armor.ArmorType.MAIL,
                34,43,43,6);

        Armor levelThreeMailLegs = new Armor("Desert herocli.hero.characters.Warrior Legs", 3,
                Item.Slot.LEGS, Armor.ArmorType.MAIL,
                55,94,54,7);

        //• Mail Body
        Armor levelOneMailBody = new Armor("Squire's Chain", 1,
                Item.Slot.BODY, Armor.ArmorType.MAIL,
                6,13,32,8);

        Armor levelTwoMailBody = new Armor("Exquisite Chieftain's Top", 2,
                Item.Slot.BODY, Armor.ArmorType.MAIL,
                34,34,44,9);

        Armor levelThreeMailBody = new Armor("Battle-worn Seafarer's Mail Body", 3,
                Item.Slot.BODY, Armor.ArmorType.MAIL,
                55,434,54,11);

        //• Plate
        //• Plate Helmet
        Armor levelOnePlateHelmet = new Armor("Brute's Visor", 1,
                Item.Slot.HEAD, Armor.ArmorType.PLATE,
                14,35,11,4);

        Armor levelTwoPlateHelmet = new Armor("Merchant's Visor", 2,
                Item.Slot.HEAD, Armor.ArmorType.PLATE,
                344,41,21,5);

        Armor levelThreePlateHelmet = new Armor("King's Guard Visor", 3,
                Item.Slot.HEAD, Armor.ArmorType.PLATE,
                445,91,42,6);

        //• Plate Legs
        Armor levelOnePlateLegs = new Armor("Brute's Legs", 1,
                Item.Slot.LEGS, Armor.ArmorType.PLATE,
                64,31,23,4);

        Armor levelTwoPlateLegs = new Armor("Merchant's Legs", 2,
                Item.Slot.LEGS, Armor.ArmorType.PLATE,
                44,43,43,6);

        Armor levelThreePlateLegs = new Armor("King's Guard Battle Bottom", 3,
                Item.Slot.LEGS, Armor.ArmorType.PLATE,
                544,94,54,7);

        //• Plate Body
        Armor levelOnePlateBody = new Armor("Brute's Protective Top", 1,
                Item.Slot.BODY, Armor.ArmorType.PLATE,
                61,13,32,8);

        Armor levelTwoPlateBody = new Armor("Merchant's Intricate Top", 2,
                Item.Slot.BODY, Armor.ArmorType.PLATE,
                412,34,44,9);

        Armor levelThreePlateBody = new Armor("King's Guard Battle Top", 3,
                Item.Slot.BODY, Armor.ArmorType.PLATE,
                521,434,54,11);

        //ADDING ITEMS TO HASH MAP

        // MAIL
        armorList.put(0, levelOneMailHelmet);
        armorList.put(1, levelTwoMailHelmet);
        armorList.put(2, levelThreeMailHelmet);

        armorList.put(3, levelOneMailBody);
        armorList.put(4, levelTwoMailBody);
        armorList.put(5, levelThreeMailBody);

        armorList.put(6, levelOneMailLegs);
        armorList.put(7, levelTwoMailLegs);
        armorList.put(8, levelThreeMailLegs);

        //CLOTH
        armorList.put(9, levelOneClothHelmet);
        armorList.put(10, levelTwoClothHelmet);
        armorList.put(11, levelThreeClothHelmet);

        armorList.put(12, levelOneClothBody);
        armorList.put(13, levelTwoClothBody);
        armorList.put(14, levelThreeClothBody);

        armorList.put(15, levelOneClothLegs);
        armorList.put(16, levelTwoClothLegs);
        armorList.put(17, levelThreeClothLegs);

        //PLATE
        armorList.put(18, levelOnePlateHelmet);
        armorList.put(19, levelTwoPlateHelmet);
        armorList.put(20, levelThreePlateHelmet);

        armorList.put(21, levelOnePlateBody);
        armorList.put(22, levelTwoPlateBody);
        armorList.put(23, levelThreePlateBody);

        armorList.put(24, levelOnePlateLegs);
        armorList.put(25, levelTwoPlateLegs);
        armorList.put(26, levelThreePlateLegs);

        //LEATHER
        armorList.put(27, levelOneLeatherHelmet);
        armorList.put(28, levelTwoLeatherHelmet);
        armorList.put(29, levelThreeLeatherHelmet);

        armorList.put(30, levelOneLeatherBody);
        armorList.put(31, levelTwoLeatherBody);
        armorList.put(32, levelThreeLeatherBody);

        armorList.put(33, levelOneLeatherLegs);
        armorList.put(34, levelTwoLeatherLegs);
        armorList.put(35, levelThreeLeatherLegs);
    }

    //print items in hash map with name and level information
    public void printList() {
        String slot = "";
        //for every item in the map
        for (Map.Entry<Integer, Armor> entry : armorList.entrySet()) {
            //determine the slot name
            switch (entry.getValue().slot) {
                case HEAD -> slot = "WEAPON";
                case BODY -> slot = "BODY";
                case LEGS -> slot = "LEGS";
                default -> slot = "";
            }
            //print the information to console
            System.out.println(
                    entry.getKey()
                            + " => "
                            + String.format("%s, Level %s %s",
                            entry.getValue().name,
                            entry.getValue().requiredLevel,
                            entry.getValue().armorType,
                            slot));
        }
    }
}

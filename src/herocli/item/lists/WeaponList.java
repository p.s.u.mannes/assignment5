package herocli.item.lists;

import herocli.item.Item;
import herocli.item.Weapon;

import java.util.HashMap;
import java.util.Map;

public class WeaponList {
    //Available types include:
    //• Axes
    //• Bows
    //• Daggers
    //• Hammers
    //• Staffs
    //• Swords
    //• Wands

    //initializing map for weapon list
    public HashMap<Integer, Weapon> weaponList = new HashMap<>();

    //constructor
    public WeaponList() {

        //• Axes
        Weapon levelOneAxe = new Weapon("Toy Axe", 1, Item.Slot.WEAPON,
                Weapon.WeaponType.AXE, 1.3, 4);

        Weapon levelTwoAxe = new Weapon("Sturdy Axe", 2, Item.Slot.WEAPON,
                Weapon.WeaponType.AXE, 2.1, 8);

        Weapon levelThreeAxe = new Weapon("Big Axe", 3, Item.Slot.WEAPON,
                Weapon.WeaponType.AXE, 3.4, 26);

        //• Bows
        Weapon levelOneBow = new Weapon("Toy Bow", 1, Item.Slot.WEAPON,
                Weapon.WeaponType.BOW, 1.3, 4);

        Weapon levelTwoBow = new Weapon("Elven Longbow", 2, Item.Slot.WEAPON,
                Weapon.WeaponType.BOW, 2.1, 8);

        Weapon levelThreeBow = new Weapon("Bow of Mighty", 3, Item.Slot.WEAPON,
                Weapon.WeaponType.BOW, 3.4, 26);

        //• Daggers
        Weapon levelOneDagger = new Weapon("Hope's End", 1, Item.Slot.WEAPON,
                Weapon.WeaponType.DAGGER, 1.3, 4);

        Weapon levelTwoDagger = new Weapon("Remorse, Voice of Trembling Hands", 2, Item.Slot.WEAPON,
                Weapon.WeaponType.DAGGER, 2.1, 8);

        Weapon levelThreeDagger = new Weapon("Saber of Assassins", 3, Item.Slot.WEAPON,
                Weapon.WeaponType.DAGGER, 3.4, 26);

        //• Hammers
        Weapon levelOneHammer = new Weapon("The Chief", 1, Item.Slot.WEAPON,
                Weapon.WeaponType.HAMMER, 1.3, 4);

        Weapon levelTwoHammer = new Weapon("Frail Obsidian Pummeler", 2, Item.Slot.WEAPON,
                Weapon.WeaponType.HAMMER, 2.1, 8);

        Weapon levelThreeHammer = new Weapon("Maneater, Annihilation of Blight", 3, Item.Slot.WEAPON,
                Weapon.WeaponType.HAMMER, 3.4, 26);

        //• Staffs
        Weapon levelOneStaff = new Weapon("Howling Pole", 1, Item.Slot.WEAPON,
                Weapon.WeaponType.STAFF, 1.3, 4);

        Weapon levelTwoStaff = new Weapon("Ominous Spiritstaff", 2, Item.Slot.WEAPON,
                Weapon.WeaponType.STAFF, 2.1, 8);

        Weapon levelThreeStaff = new Weapon("Rune-Forged Warden Staff", 3, Item.Slot.WEAPON,
                Weapon.WeaponType.STAFF, 3.4, 26);

        //• Swords
        Weapon levelOneSword = new Weapon("Faith's Quickblade", 1, Item.Slot.WEAPON,
                Weapon.WeaponType.SWORD, 1.3, 4);

        Weapon levelTwoSword = new Weapon("Proud Sabre", 2, Item.Slot.WEAPON,
                Weapon.WeaponType.SWORD, 2.1, 8);

        Weapon levelThreeSword = new Weapon("Isolated Katana", 3, Item.Slot.WEAPON,
                Weapon.WeaponType.SWORD, 3.4, 26);

        //• Wands
        Weapon levelOneWand = new Weapon("Vindicator", 1, Item.Slot.WEAPON,
                Weapon.WeaponType.WAND, 1.3, 4);

        Weapon levelTwoWand = new Weapon("Inertia, Terror of Zeal", 2, Item.Slot.WEAPON,
                Weapon.WeaponType.WAND, 2.1, 8);

        Weapon levelThreeWand = new Weapon("Spellkeeper, Wand of Diligence", 3, Item.Slot.WEAPON,
                Weapon.WeaponType.WAND, 3.4, 26);

        //ADDING WEAPONS TO MAP

        //Axes
        weaponList.put(0, levelOneAxe);
        weaponList.put(1, levelTwoAxe);
        weaponList.put(2, levelThreeAxe);

        //Bows
        weaponList.put(3, levelOneBow);
        weaponList.put(4, levelTwoBow);
        weaponList.put(5, levelThreeBow);

        //Daggers
        weaponList.put(6, levelOneDagger);
        weaponList.put(7, levelTwoDagger);
        weaponList.put(8, levelThreeDagger);

        //Hammers
        weaponList.put(9, levelOneHammer);
        weaponList.put(10, levelTwoHammer);
        weaponList.put(11, levelThreeHammer);

        //Swords
        weaponList.put(12, levelOneSword);
        weaponList.put(13, levelTwoSword);
        weaponList.put(14, levelThreeSword);

        //Staves
        weaponList.put(15, levelOneStaff);
        weaponList.put(16, levelTwoStaff);
        weaponList.put(17, levelThreeStaff);

        //Wands
        weaponList.put(18, levelOneWand);
        weaponList.put(19, levelTwoWand);
        weaponList.put(20, levelThreeWand);
    }

    //Method to print the list
    public void printList() {
        //Loops through every K, V pair in the map
        //and returns weapon-specific information
        for (Map.Entry<Integer, Weapon> entry : weaponList.entrySet()) {
            System.out.println(
                    entry.getKey()
                            + " => "
                            + String.format("%s, Level %s %s",
                            entry.getValue().name,
                            entry.getValue().requiredLevel,
                            entry.getValue().weaponType));
        }
    }
}

package herocli.item;

public abstract class Item {
    //name and required level to equip item
    public String name;
    public int requiredLevel;

    //slot enum to indicate the item slot
    public enum Slot {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }

    public Slot slot;

    //constructor
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }
}

package herocli.item;

import herocli.hero.PrimaryAttribute;

public class Armor extends Item {

    // Creating enum for armor type
    public enum ArmorType {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE,
    }

    //initializing enum
    public ArmorType armorType;

    //initializing armor attribute
    private PrimaryAttribute armorAttribute;

    //getter for armor attribute
    public PrimaryAttribute getArmorAttribute() {
        return armorAttribute;
    }

    //constructor using all class fields and superclass fields
    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType,
                 int vitality, int strength, int dexterity, int intelligence) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.armorAttribute = new PrimaryAttribute(vitality, strength, dexterity, intelligence);
    }

}

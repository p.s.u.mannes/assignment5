package herocli.hero;

public class PrimaryAttribute {

    // getters
    public int getVitality() {
        return vitality;
    }
    public int getStrength() {
        return strength;
    }
    public int getDexterity() {
        return dexterity;
    }
    public int getIntelligence() {
        return intelligence;
    }

    // set primary attribute
    public void setPrimaryAttribute(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // fields
    private int vitality;
    private int strength;
    private int dexterity;
    private int intelligence;

    // constructor
    public PrimaryAttribute(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // String method for complex object helps
    // with testing
    @Override
    public String toString() {
        return "PrimaryAttribute{" +
                "vitality=" + vitality +
                ", strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                '}';
    }
}

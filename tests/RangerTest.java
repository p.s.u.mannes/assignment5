import herocli.hero.characters.Ranger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// One assert per test method (strive for this)
// Remember AAA – arrange act assert.
// Be as explicit as possible, every parameter or output is stored in a variable.
// Do not invoke the method in the assert, invoke it before and save the return.
// Naming is important.
// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().

class RangerTest {

    @Test
    void Ranger_whenCreated_ShouldBeLevel1() {
        // Arrange
        Ranger ranger = new Ranger();
        int expected = 1;

        // Act
        int actual = ranger.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Ranger_whenCreated_ShouldHaveDefaultAttributes() {
        // Arrange
        Ranger ranger = new Ranger();
        int expectedVitality = 8;
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        int[] actual = {ranger.getPrimaryAttribute().getVitality(),
                ranger.getPrimaryAttribute().getStrength(),
                ranger.getPrimaryAttribute().getDexterity(),
                ranger.getPrimaryAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_voidMethodCallOnNewCharacter_ShouldResultInCharacterLevel2() {
        Ranger ranger = new Ranger();
        int expected = 2;

        //Act
        ranger.levelUp();
        int actual = ranger.getLevel();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_voidMethodCall_ShouldIncreaseAttributes() {
        // Arrange
        Ranger ranger = new Ranger();
        int expectedVitality = 10;
        int expectedStrength = 2;
        int expectedDexterity = 12;
        int expectedIntelligence = 2;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        ranger.levelUp();
        int[] actual = {ranger.getPrimaryAttribute().getVitality(),
                ranger.getPrimaryAttribute().getStrength(),
                ranger.getPrimaryAttribute().getDexterity(),
                ranger.getPrimaryAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }
}
import herocli.hero.characters.Rogue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// One assert per test method (strive for this)
// Remember AAA – arrange act assert.
// Be as explicit as possible, every parameter or output is stored in a variable.
// Do not invoke the method in the assert, invoke it before and save the return.
// Naming is important.
// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().

class RogueTest {

    @Test
    void Rogue_whenCreated_ShouldBeLevel1() {
        // Arrange
        Rogue rogue = new Rogue();
        int expected = 1;

        // Act
        int actual = rogue.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Rogue_whenCreated_ShouldHaveDefaultAttributes() {
        // Arrange
        Rogue rogue = new Rogue();
        int expectedVitality = 8;
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        int[] actual = {rogue.getPrimaryAttribute().getVitality(),
                rogue.getPrimaryAttribute().getStrength(),
                rogue.getPrimaryAttribute().getDexterity(),
                rogue.getPrimaryAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_voidMethodCallOnNewCharacter_ShouldResultInCharacterLevel2() {
        Rogue rogue = new Rogue();
        int expected = 2;

        //Act
        rogue.levelUp();
        int actual = rogue.getLevel();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_voidMethodCall_ShouldIncreaseAttributes() {
        // Arrange
        Rogue rogue = new Rogue();
        int expectedVitality = 11;
        int expectedStrength = 3;
        int expectedDexterity = 10;
        int expectedIntelligence = 2;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        rogue.levelUp();
        int[] actual = {rogue.getPrimaryAttribute().getVitality(),
                rogue.getPrimaryAttribute().getStrength(),
                rogue.getPrimaryAttribute().getDexterity(),
                rogue.getPrimaryAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }
}
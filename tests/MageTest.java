import herocli.hero.characters.Mage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// One assert per test method (strive for this)
// Remember AAA – arrange act assert.
// Be as explicit as possible, every parameter or output is stored in a variable.
// Do not invoke the method in the assert, invoke it before and save the return.
// Naming is important.
// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().

class MageTest {

    @Test
    void Mage_whenCreated_ShouldBeLevel1() {
        // Arrange
        Mage mage = new Mage();
        int expected = 1;

        // Act
        int actual = mage.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Mage_whenCreated_ShouldHaveDefaultAttributes() {
        // Arrange
        Mage mage = new Mage();
        int expectedVitality = 5;
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        int[] actual = {mage.getPrimaryAttribute().getVitality(),
                mage.getPrimaryAttribute().getStrength(),
                mage.getPrimaryAttribute().getDexterity(),
                mage.getPrimaryAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_voidMethodCallOnNewCharacter_ShouldResultInCharacterLevel2() {
        Mage mage = new Mage();
        int expected = 2;

        //Act
        mage.levelUp();
        int actual = mage.getLevel();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_voidMethodCall_ShouldIncreaseAttributes() {
        // Arrange
        Mage mage = new Mage();
        int expectedVitality = 8;
        int expectedStrength = 2;
        int expectedDexterity = 2;
        int expectedIntelligence = 13;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        mage.levelUp();
        int[] actual = {mage.getPrimaryAttribute().getVitality(),
                mage.getPrimaryAttribute().getStrength(),
                mage.getPrimaryAttribute().getDexterity(),
                mage.getPrimaryAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }
}
import herocli.hero.Character;
import herocli.hero.characters.Warrior;
import herocli.item.Armor;
import herocli.item.Item;
import herocli.item.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// One assert per test method (strive for this)
// Remember AAA – arrange act assert.
// Be as explicit as possible, every parameter or output is stored in a variable.
// Do not invoke the method in assert, invoke it before and save the return.
// Naming is important.
// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().

public class ItemTest {
    // To test for exceptions:

    // assertThrows( com.herokcliapp.herocli.hero.Character.InvalidWeaponException.class, () -> {
    //    warrior.equipWeapon(testAxe);
    // });

    // However, all exceptions are caught and handled in the code to
    // stop the application from breaking. The exceptions do not reach the interpreter.
    // Therefore, only the return boolean is tested for in these tests.

    // Removing the catch functionality from com.herokcliapp.herocli.hero.Character.equipArmorHandler and
    // com.herokcliapp.herocli.hero.Character.equipArmorHandler will result in assertThrows tests passing,
    // at the cost of unhandled exceptions breaking the application. For these reasons,
    // all exceptions are caught.

    @Test
    void equipWeapon_weaponIsTooHighLevel_ShouldThrowInvalidWeaponExceptionAndReturnFalse() throws Character.InvalidWeaponException {
        // Arrange
        Warrior warrior = new Warrior();
        Weapon testAxe = new Weapon("Test Axe",
                2,
                Item.Slot.WEAPON,
                Weapon.WeaponType.AXE,
                0,
                0);
        boolean expected = false;

        // Act
        boolean actual = warrior.equipWeapon(testAxe);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_weaponTypeIsInvalid_ShouldThrowInvalidWeaponExceptionAndReturnFalse() throws Character.InvalidWeaponException {
        // Arrange
        Warrior warrior = new Warrior();
        Weapon testBow = new Weapon("Test Bow",
                1,
                Item.Slot.WEAPON,
                Weapon.WeaponType.BOW,
                3.4,
                2);
        boolean expected = false;

        // Act
        boolean actual = warrior.equipWeapon(testBow);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_weaponTypeIsValid_ShouldReturnTrue() throws Character.InvalidWeaponException {
        // Arrange
        Warrior warrior = new Warrior();
        Weapon testAxe = new Weapon("Test Axe",
                1,
                Item.Slot.WEAPON,
                Weapon.WeaponType.AXE,
                0,
                0);

        boolean expected = true;

        // Act
        boolean actual = warrior.equipWeapon(testAxe);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_armorIsTooHighLevel_ShouldThrowInvalidArmorExceptionAndReturnFalse() throws Character.InvalidArmorException {
        // Arrange
        Warrior warrior = new Warrior();
        Armor testPlateBody = new Armor("Test Plate Body",
                2,
                Item.Slot.BODY,
                Armor.ArmorType.PLATE,
                0,
                0,
                0,
                0);

        boolean expected = false;

        // Act
        boolean actual = warrior.equipArmor(testPlateBody);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_armorTypeIsInvalid_ShouldThrowInvalidArmorExceptionAndReturnFalse() throws Character.InvalidArmorException {
        // Arrange
        Warrior warrior = new Warrior();
        Armor testClothBody = new Armor("Test Cloth Body",
                1,
                Item.Slot.BODY,
                Armor.ArmorType.CLOTH,
                0,
                0,
                0,
                0);

        boolean expected = false;

        // Act
        boolean actual = warrior.equipArmor(testClothBody);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_armorTypeIsValid_ShouldReturnTrue() throws Character.InvalidArmorException {
        // Arrange
        Warrior warrior = new Warrior();
        Armor testPlateBody = new Armor("Test Plate Body",
                1,
                Item.Slot.BODY,
                Armor.ArmorType.PLATE,
                0,
                0,
                0,
                0);

        boolean expected = true;

        // Act
        boolean actual = warrior.equipArmor(testPlateBody);

        // Assert
        assertEquals(expected, actual);
    }

    //Calculate DPS if no weapon is equipped.
    @Test
    void getCharacterDPS_nothingIsEquipped_ShouldReturnValidDouble() {
        // Arrange
        Warrior warrior = new Warrior();
        double expected = 1*(1 + (5 / 100.0));

        // Act
        double actual = warrior.getCharacterDPS();

        // Assert
        assertEquals(expected, actual);
    }

    //Calculate DPS with valid weapon equipped.
    @Test
    void getCharacterDPS_validWeaponIsEquipped_ShouldReturnValidDouble() throws Character.InvalidWeaponException {
        // Arrange
        Warrior warrior = new Warrior();
        Weapon testAxe = new Weapon("Test Axe",
                1,
                Item.Slot.WEAPON,
                Weapon.WeaponType.AXE,
                1.1,
                7);

        // DPS = (weaponDamage * weaponSpeed) * ( 1 + (totalPrimaryAttribute / 100.0) )
        double expected = (7 * 1.1)*(1 + (5 / 100.0));

        // Act
        warrior.equipWeapon(testAxe);
        double actual = warrior.getCharacterDPS();

        // Assert
        assertEquals(expected, actual);
    }

    //Calculate DPS with valid weapon and armor equipped.
    @Test
    void getCharacterDPS_validWeaponAndArmorIsEquipped_ShouldReturnValidDouble() throws Character.InvalidWeaponException, Character.InvalidArmorException {
        // Arrange
        Warrior warrior = new Warrior();
        Weapon testAxe = new Weapon("Test Axe",
                1,
                Item.Slot.WEAPON,
                Weapon.WeaponType.AXE,
                1.1,
                7);

        Armor testPlateBody = new Armor("Test Plate Body",
                1,
                Item.Slot.BODY,
                Armor.ArmorType.PLATE,
                2,
                1,
                0,
                0);

        // DPS = (weaponDamage * weaponSpeed) * ( 1 + (totalPrimaryAttribute / 100.0) )
        double expected = (7 * 1.1) * (1 + ((5+1) / 100.0));

        // Act
        warrior.equipWeapon(testAxe);
        warrior.equipArmor(testPlateBody);
        double actual = warrior.getCharacterDPS();

        // Assert
        assertEquals(expected, actual);
    }

    // Test if equipping armor increases stats correctly
    @Test
    void getTotalAttribute_validArmorIsEquipped_ShouldReturnValidDouble() throws Character.InvalidArmorException {
        // Arrange
        Warrior warrior = new Warrior();
        Armor testPlateBody = new Armor("Test Plate Body",
                1,
                Item.Slot.BODY,
                Armor.ArmorType.PLATE,
                2,
                1,
                0,
                0);

        int expectedVitality = 12;
        int expectedStrength = 6;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        warrior.equipArmor(testPlateBody);
        int[] actual = {warrior.getTotalAttribute().getVitality(),
                warrior.getTotalAttribute().getStrength(),
                warrior.getTotalAttribute().getDexterity(),
                warrior.getTotalAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }
}

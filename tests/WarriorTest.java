import herocli.hero.characters.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// One assert per test method (strive for this)
// Remember AAA – arrange act assert.
// Be as explicit as possible, every parameter or output is stored in a variable.
// Do not invoke the method in the assert, invoke it before and save the return.
// Naming is important.
// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().

class WarriorTest {

    @Test
    void Warrior_whenCreated_ShouldBeLevel1() {
        // Arrange
        Warrior warrior = new Warrior();
        int expected = 1;

        // Act
        int actual = warrior.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Warrior_whenCreated_ShouldHaveDefaultAttributes() {
        // Arrange
        Warrior warrior = new Warrior();
        int expectedVitality = 10;
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        int[] actual = {warrior.getPrimaryAttribute().getVitality(),
                warrior.getPrimaryAttribute().getStrength(),
                warrior.getPrimaryAttribute().getDexterity(),
                warrior.getPrimaryAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_voidMethodCallOnNewCharacter_ShouldResultInCharacterLevel2() {
        Warrior warrior = new Warrior();
        int expected = 2;

        //Act
        warrior.levelUp();
        int actual = warrior.getLevel();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_voidMethodCall_ShouldIncreaseAttributes() {
        // Arrange
        Warrior warrior = new Warrior();
        int expectedVitality = 15;
        int expectedStrength = 8;
        int expectedDexterity = 4;
        int expectedIntelligence = 2;
        int[] expected = {expectedVitality, expectedStrength, expectedDexterity, expectedIntelligence};

        // Act
        warrior.levelUp();
        int[] actual = {warrior.getPrimaryAttribute().getVitality(),
                warrior.getPrimaryAttribute().getStrength(),
                warrior.getPrimaryAttribute().getDexterity(),
                warrior.getPrimaryAttribute().getIntelligence()};

        // Assert
        assertArrayEquals(expected, actual);
    }
}
<h1 align="center">Welcome to HeroCLI 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
</p>

> A simple CLI application written in Java to create, level, and equip heroes

                                      /|
                                     |\|
                                     |||
                                     |||
                                     |||
                                     |||
                                     |||
                                     |||
                                  ~-[{o}]-~
                                     |/|
                                     |/|
             ///~`     |\\_          `0'         =\\\\         . .
            ,  |='  ,))\_| ~-_                    _)  \      _/_/|
           / ,' ,;((((((    ~ \                  `~~~\-~-_ /~ (_/\
         /' -~/~)))))))'\_   _/'                      \_  /'  D   |
        (       (((((( ~-/ ~-/                          ~-;  /    \--_
         ~~--|   ))''    ')  `                            `~~\_    \   )
             :        (_  ~\           ,                    /~~-     ./

_ASCII art by Tua Xiong_

## Usage

```
Setup libraries, JDK16, and JUnit for testing
src/herocli/Main --->
run Main.main() in IDE or compile via CLI if desired
```

## Run tests

```sh
All tests are run inside of the tests folder.
```

Usage:
- Choose a character class
- Choose a username
- View lists of armor/weapons
- Show stats
- Level up
- Equip items
- Automated testing


Limitations:
- CLI only, no GUI
- No information is saved upon quitting the app



***
_END OF README FILE_


